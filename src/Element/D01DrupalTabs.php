<?php

namespace Drupal\d01_drupal_tabs\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * A tabs render element.
 *
 * @RenderElement("d01_drupal_tabs")
 */
class D01DrupalTabs extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_tabs',
      '#element_id' => FALSE,
      '#element_type' => FALSE,
      '#js_settings' => [],
      '#attributes' => [],
      '#items' => [],
      '#navigation' => [],
      '#attached' => [
        'library' => [
          'd01_drupal_tabs/tabs',
        ],
        'drupalSettings' => [
          'd01_drupal_tabs' => [],
        ],
      ],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      // For first version we don't cache the tabs.
      // and his content. This will be fixed in future updates.
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Prerender function for the tab element.
   */
  public static function preRenderElement($element) {
    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need a #tabs_id for theme suggestions and js-settings.
    // This way we can have more than one element on the page with different
    // settings.
    if (!$element['#element_id']) {
      $element = [
        '#markup' => t('The d01_drupal_tabs element requires an #element_id to work.'),
      ];
      return $element;
    }

    // We need to make sure all children added in #items
    // are d01_drupal_tabs_items items. So we remove all
    // other items.
    $count = 0;

    if (!empty($element['#items'])) {
      foreach ($element['#items'] as $key => $child) {
        if (!D01DrupalTabsItem::isD01DrupalTabsItem($child)) {
          unset($element['#items'][$key]);
        }
        else {
          // Add the parent element_id and the position of the item to
          // the children. This way we can provide more specific
          // theme suggestions.
          $element['#items'][$key]['#attributes']['id'] = 'js-tabs-item-' . $key;
          $element['#items'][$key]['#position'] = $count;
          $element['#items'][$key]['#element_type'] = $element['#element_type'];
          $count++;
        }
      }
    }

    // Convert the #navigation to render array.
    $element['navigation'] = !empty($element['#navigation']) ? $element['#navigation'] : [];

    // Convert #items to a renderable element.
    $element['items'] = !empty($element['#items']) ? $element['#items'] : [];

    // Get the #element_id and the #js_settings.
    $js_id = $element['#element_id'];
    $js_settings = is_array($element['#js_settings']) ? $element['#js_settings'] : [];

    // Set the #element_id as html Id.
    $element['#attributes']['id'] = $js_id;

    // Add required JS classes.
    $element['#attributes']['class'][] = 'js-d01-drupal-tabs';

    // Pass the settings to js keyed by the #element_id.
    $element['#attached']['drupalSettings']['d01_drupal_tabs'][$js_id] = $js_settings;

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}
