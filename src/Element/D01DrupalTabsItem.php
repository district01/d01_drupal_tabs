<?php

namespace Drupal\d01_drupal_tabs\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * A tabs render element.
 *
 * @RenderElement("d01_drupal_tabs_item")
 */
class D01DrupalTabsItem extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_tabs_item',
      '#element_type' => FALSE,
      '#attributes' => [],
      '#item' => [],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      // For first version we don't cache the tabs.
      // and his content. This will be fixed in future updates.
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Prerender function for the content item element.
   */
  public static function preRenderElement($element) {
    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need a render array for our tabs item element
    // but we want to give people full freedom to pass whatever
    // they want to the element.So we need to check if we recieve
    // a render array and else we need to convert it to a render array.
    if (!D01DrupalTabsItem::isRenderableArray($element['#item'])) {
      // Convert string to render array.
      $element['#item'] = ['#markup' => $element['#item']];
    }

    // Convert #content to a renderable elements.
    $element['item'] = $element['#item'];

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

  /**
   * Is renderable array.
   *
   * Check if the passed item is a render array.
   *
   * @param mixed $element
   *   A item to check.
   *
   * @return bool
   *   A boolean indicating if item contains #theme, #type or #markup.
   */
  private static function isRenderableArray($element) {

    // Check if we got an array.
    if (!is_array($element)) {
      return FALSE;
    }

    // Check for #theme or #type key.
    if (isset($element['#theme']) || isset($element['#type']) || isset($element['#markup'])) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Is tabs item.
   *
   * Check if the passed item is a render
   * array that is an implementation of
   * d01_drupal_tabs_item.
   *
   * @param mixed $element
   *   A item to check.
   *
   * @return bool
   *   A boolean indicating if item is of type d01_drupal_tabs_item.
   */
  public static function isD01DrupalTabsItem($element) {

    // Check if array.
    if (!is_array($element)) {
      return FALSE;
    }

    // Check if it's a render element.
    if (!isset($element['#type'])) {
      return FALSE;
    }

    // Check if it's a d01_drupal_accordion_item.
    if ($element['#type'] !== 'd01_drupal_tabs_item') {
      return FALSE;
    }

    return TRUE;
  }

}
