<?php

namespace Drupal\d01_drupal_tabs\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * A tabs render element.
 *
 * @RenderElement("d01_drupal_tabs_navigation")
 */
class D01DrupalTabsNavigation extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_tabs_navigation',
      '#element_type' => FALSE,
      '#attributes' => [],
      '#items' => [],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      // For first version we don't cache the tabs.
      // and his content. This will be fixed in future updates.
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Prerender function for the content item element.
   */
  public static function preRenderElement($element) {
    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need to make sure all children added in #items
    // are d01_drupal_tabs_navigation_item items. So we remove all
    // other items.
    $count = 0;
    if (!empty($element['#items'])) {
      foreach ($element['#items'] as $key => $child) {
        if (!D01DrupalTabsNavigationTab::isD01DrupalTabsNavigationTab($child)) {
          unset($element['#items'][$key]);
        }
        else {

          // Add the parent element_id and the position of the item to
          // the children. This way we can provide more specific
          // theme suggestions.
          $element['#items'][$key]['#attributes']['href'] = '#js-tabs-item-' . $key;
          $element['#items'][$key]['#position'] = $count;
          $element['#items'][$key]['#element_type'] = $element['#element_type'];
          $count++;
        }
      }
    }

    // Convert the items to render arrays.
    $element['items'] = $element['#items'];

    // Mark as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}
