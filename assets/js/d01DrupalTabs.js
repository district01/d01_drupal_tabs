(function ($) {

  'use strict';

  /**
   * jQuery object
   * @external jQuery
   * @see {@link http://api.jquery.com/jQuery/}
   */

  /**
   * D01DrupalTabs
   *
   * @param {string} wrapperClass
   *    css class of the tabs wrapper.
   * @param {jQuery} context
   *    a jQuery element containing the context.
   * @param {object} drupalSettings
   *    an object containing all settings.
   * @constructor
   */
  var D01DrupalTabs = function (wrapperClass, context, drupalSettings) {
    var that = this;
    that.wrapperClass = wrapperClass;
    that.context = context;
    that.settings = drupalSettings.d01_drupal_tabs || {};

    // Store the window width
    that.windowWidth = $(window).width();
    that.isResizing = false;
    that.addEventListeners();
  };

  /**
   * addEventListeners().
   */
  D01DrupalTabs.prototype.addEventListeners = function () {
    var that = this;
    window.addEventListener('resize', that.resizeEvent.bind(that), false);
  };

  /**
   * resizeEvent().
   */
  D01DrupalTabs.prototype.resizeEvent = function () {
    var that = this;

    // Check window width has actually changed
    // and it's not just iOS triggering a resize event on scroll
    if ($(window).width() !== that.windowWidth) {
      that.windowWidth = $(window).width();
      that.windowResized();
    }
  };

  /**
   * windowResized().
   */
  D01DrupalTabs.prototype.windowResized = function () {
    var that = this;

    // Use requestAnimationFrame to debounce resize event.
    if (!that.isResizing) {
      requestAnimationFrame(that.onResize.bind(that));
    }

    that.isResizing = true;
  };

  /**
   * onResize().
   */
  D01DrupalTabs.prototype.onResize = function () {
    var that = this;
    that.isResizing = false;
    that.initializeTabs();
  };

  /**
   * initializeTabs().
   */
  D01DrupalTabs.prototype.initializeTabs = function () {
    var that = this;

    $(that.wrapperClass).each(function () {
      var tabs = $(this);
      var tabsId = tabs.attr('id');
      var tabsSettings = that.settings[tabsId] || {};

      // Pass create event to document.
      tabsSettings.activate = function() {
        $.event.trigger("D01DrupalTabs:create", tabs);
      };

      // Pass activate event to document.
      tabsSettings.activate = function() {
        $.event.trigger("D01DrupalTabs:activate", tabs);
      };

      // Pass beforeActivate event to document.
      tabsSettings.beforeActivate = function() {
        $.event.trigger("D01DrupalTabs:beforeActivate", tabs);
      };

      // Pass beforeLoad event to document.
      tabsSettings.beforeLoad = function() {
        $.event.trigger("D01DrupalTabs:beforeLoad", tabs);
      };

      // Pass load event to document.
      tabsSettings.load = function() {
        $.event.trigger("D01DrupalTabs:load", tabs);
      };

      // Enable tabs aren't active but should be.
      $('#' + tabsId).tabs(tabsSettings);
    });
  };

  /**
   * @type {D01DrupalTabs}
   */
  window.D01DrupalTabs = D01DrupalTabs;

})(jQuery);
