/**
 * d01DrupalTabs.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * D01 Drupal Tabs behavior.
   * @type {{attach: Drupal.behaviors.d01_drupal_tabs.attach}}
   */
  Drupal.behaviors.d01_drupal_tabs = {
    attach: function (context, drupalSettings) {
      new D01DrupalTabs(
        '.js-d01-drupal-tabs',
        context,
        drupalSettings
      ).initializeTabs();
    }
  };
})(jQuery, Drupal);